<?php
//Configuration
$url = 'http://www.ariacharts.com.au/charts/singles-chart';
$table_selector='table#tbChartItems tbody';
$fields=array(
  'rank'=>'td.ranking span',
  'title'=>'div.item-title',
  'artist'=>'div.artist-name',
);
$output_filename="result.csv";
