<?php
require 'vendor/autoload.php';
require_once('class.aria-scraper.php');
require_once('config.php');

$aria_scraper= new AriaScraper();

if($aria_scraper->retrieve_table_fields($url,$table_selector,$fields)){
  echo "Records retrieved Successfully. Generating CSV File". PHP_EOL;
  if($aria_scraper->print_to_csv($output_filename)){
    $aria_scraper->print_to_screen();
    echo "CSV file generated Successfully.". PHP_EOL;
  }
  else {
      echo "Error. CSV file has not been generated.". PHP_EOL;
  }
}
else {
  echo "Error. No records were retrieved.". PHP_EOL;
}
