<?php
/** Class to read charts from ARIA Charts website and retrieve them in CSV format
*  @author MY Fallaha <myfallaha@gmail.com>
*/

/**
* This class uses Goutte simple PHP web scraper tool that provides a API
* to crawl websites and extract data from the HTML/XML responses.
* https://github.com/FriendsOfPHP/Gouttee
*/
use Goutte\Client;
class AriaScraper{
  //Fields to be retrieved from the chart table. This can be customised to add as much fields as required.
  public $fields=array();
  //An array to save the retreived records.
  public $records=array();
  //Class constructor
  function __construct() {
    //Create a Goutte Client instance (which extends Symfony\Component\BrowserKit\Client):
    $client = new Client();
    $this->client = $client;
  }
  /** A method to retreive the fields from the chart table
  * @param string $url: Source URL that containt the chart table
  * @param string $table_selector: CSS selector of the HTML table
  * @param array $fields: array of CSS selectors of the fields to be retrieved
  * @return bool: true if table records are retrieved else false
  */
  public function retrieve_table_fields($url,$table_selector,$fields){
    $this->fields=$fields;
    //Send GET request to get page content
    $crawler = $this->client->request('GET', $url);
    //Check if the target URL is working and returning HTTP code 200
    if ($this->client->getResponse()->getStatus() != 200) {
      die('The URL is not working. Please try a different URL.');
    }
    //Select table rows only
    $table_rows=$crawler->filter($table_selector)->filter('tr');
    if($table_rows){
      //Extract required fields from table rows and assign them to $records property
      $this->records = $table_rows->each(
        function ($tr, $i) {
          //Check if CSS selectors are available in the row and halt process if any of them are not correct
          try {
            foreach($this->fields as $key => $selector){
              $res[$key]=htmlspecialchars($tr->filter($selector)->text());
            }
          }
          catch (Exception $ex) {
            die('One of the fields was not extracted properly, please check css selectors.');
          }
          return $res;
        }
      );
      if($this->records)
        return true;
      }
    return false;
  }
  /** A method to output recrods to CSV file
  * @param string $filename: destination CSV file name
  * @return bool: true on success else false
  */
  public function print_to_csv($filename){
    if($this->records){
      $fp = fopen($filename, 'w');
      if($fp){
        foreach($this->records as $record)
        fputcsv($fp, array_values($record));
        fclose($fp);
        return true;
      }
    }
    return false;
  }
  /** A method to print recrods on screen
  */
  public function print_to_screen(){
    if($this->records){
      foreach($this->records as $record){
        echo implode(' - ',$record).PHP_EOL;
      }
    }
  }
}
