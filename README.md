# ARIA Charts Scraper #
A script to read charts from [ARIA Charts website](http://www.ariacharts.com.au/charts/singles-chart) and retrieve them in CSV format. 

It uses [Goutte library](https://github.com/FriendsOfPHP/Goutte) to extract data from web pages.

## Installation ##
* [Composer](http://getcomposer.org/) is required to install dependencies
* PHP 5.5+ is required to run Goutte

## Configuration ##
* Change configuration in config.php file.


